# Name of the project + Version
A brief description of the project, you should answer the question �What problem is this project solving?� Also state what the project is not for.

[project source](https://google.de)

## Contact
- The name and contact details of the client and any 3rd party vendors
- The names of the developers on the project(with email)

## Installation/General
This section should give a new developer an idea of what the project is about.

- needed system requirements (e.g.: db version, API Level, screensize/resolution, PHP version, webserver, etc.)
- An outline of the technologies in the project. e.g.: Frameworks used (iOS/Android), programming language, database, ORM.
- Links to any related projects
- Links to online tools related to the application (e.g.: link to OSM)
## Getting Started
This section outlines the process of getting the app installed and usable for a developer. Meaning 'useable' in this context as being able to login to the application and access all of the functionality available.   [example Link][df1]



- A detailed spin-up process. This should include:
Instructions on installing any software the application is dependent on: e.g.: wkhtmltopdf, PostgreSQL, XQuartz.
Instructions on running the app. Such as instructions on starting a server 
- A list of credentials that can be used to log in with each user type in the system and ideally the URL that a developer can log in from.
- Any information about subdomains in the app (e.g.: api.myapp.dev/)
- required installation steps
- steps for a brief introducing tutorial
- some FAQ

### Third Party libraries/frameworks/etc.

Projects being used in this app e.g. :

* [AngularJS] - HTML enhanced for web apps!
* [markdown-it] - Markdown parser done right. Fast and easy to extend.
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [jQuery] - duh


### Testing
include the commands to run any of the test suites you have (e.g.: RSpec, Jasmine, Cucumber, Spinach, UnitTest) and any setup you need to do before-hand (e.g.: rake db:test:prepare).


### Development
Details for developers on how to contribute to the app

### Todos

 - Write Tests
 - Rethink Github Save
 - Add Code Comments
 - Add Night Mode

License
----

MIT